import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Cores from './app/util/Cores'

//import SignIn from'./app/screens/SignIn'
//import Onboarding from './app/screens/Onboarding';
//import SignUp from './app/screens/SignUp'

import { 
  Onboarding, 
  SignIn, 
  SignUp
} from "./app/screens";


function ScreenOnboarding({navigation}){

  return(
    <Onboarding nav={navigation}/>
  )
}

function ScreenSignIn({navigation}){
  return(
    <SignIn nav={navigation}/>
  )
}

function ScreenSignUp({navigation}){
  return(
    <SignUp nav={navigation}/>
  )
}

//pilha de navegação
const pilha = createStackNavigator();

export default function App() {
  return (
    <SafeAreaView style={{width:'100%', height:'100%'}}>
      <NavigationContainer>
      <StatusBar backgroundColor={Cores.laranjaPadrao} style='light'/>
        <pilha.Navigator initialRouteName="Onboarding" //aqui a primeira tela deve ser o Splash 
          //aptions padrão para tds as telas (se n é definido title então ele será o name da tela)
          screenOptions={{headerTintColor:Cores.white, presentation:'modal',headerStyle:{backgroundColor:Cores.purple_500}}}
        >
          <pilha.Screen
            name='Onboarding'
            options={{ headerStyle:{height:0}}}//assim a toobar dessa tela n aparece
            component={ScreenOnboarding}
          />

          <pilha.Screen
            name='Login'
            options={{headerTintColor:Cores.black, title: ' ', headerStyle:{backgroundColor:Cores.white, shadowColor:Cores.white}}}
            component={ScreenSignIn}
          />

          <pilha.Screen
            name='Cadastrese'
            options={{headerTintColor:Cores.black, title: ' ', headerStyle:{backgroundColor:Cores.white, shadowColor:Cores.white}}}
            component={ScreenSignUp}
          />

        </pilha.Navigator>
      </NavigationContainer>

      
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
