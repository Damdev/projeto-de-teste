import React, { useState } from "react";
import { SafeAreaView, TextInput, TouchableHighlight, View, Text, Alert, ScrollView, Image } from "react-native";
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons'
//import { ProgressBar } from '@react-native-community/progress-bar-android';
import Cores from '../util/Cores';
import Estilos from "../util/Estilos";
import Strings from "../util/Strings";
import Button from '../meus_componentes/Button'
import Checkbox from 'expo-checkbox';
import { RadioButton } from 'react-native-paper';
//import auth from '@react-native-firebase/auth';

export default function(params){
    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    const [nomeEmpresa, setNEmpresa] = useState('')
    const [PrimeiroNome, setNome] = useState('')
    const [Sobrenome, setSobrenome] = useState('')
    const [tel, setTel] = useState('') 

    const [progtessbarsVisible, setPGBV] = useState(false)

    //estatos dos chackboxs
    const  [ toggleCheckBox1 ,  setToggleCheckBox1 ]  =  useState ( true)
    const  [ toggleCheckBox2 ,  setToggleCheckBox2 ]  =  useState ( false )
    const [isChecked1, setChecked1] = useState(false);
    const [isChecked2, setChecked2] = useState(false);
    const [isChecked3, setChecked3] = useState(false);
    const [isChecked4, setChecked4] = useState(false);


    
    //logando
    /*
    function logar(){
        setPGBV(true)
        if(email == '' || senha == ''){
            Alert.alert(Strings.algo_errado, Strings.digite_email_senha)
            setPGBV(false)
        }else{
            auth()
            .signInWithEmailAndPassword(email, senha)
            .then(() => {
                console.log('User account created & signed in!');
                setPGBV(false)
                params.nav.dispatch(
                    CommonActions.navigate({
                      name: 'Splash',
                      params: {
                        //nav: params.nav,
                      },
                    })
                  );
                
            })
            .catch(error => {   
                var mensagem = error.code
                
                if (error.code === 'auth/invalid-email') {
                mensagem = Strings.email_errado
                }if(error.code === 'auth/wrong-password'){
                    mensagem = Strings.senha_incorreta
                }
                Alert.alert(Strings.algo_errado, mensagem)
                setPGBV(false)

            });
        }
    }*/

    return(
        <ScrollView style={{flex:1, height:'100%', backgroundColor:Cores.white}}>
            <SafeAreaView style={{flex:1, backgroundColor:Cores.white}}>
                
                <Image
                    style={{width:'70%', height:70, alignSelf:'center'}}
                    resizeMode='contain'
                    source={require('../imagens/logo.png')}
                />

                
                <View style={{flexDirection:'row', alignItems:'center', marginStart:12, marginEnd:12}}>
                
                    <View  style={{flexDirection:'row',alignItems: 'center', justifyContent:'flex-end', flex:1, alignSelf:'flex-start'}}>
                        <RadioButton
                            value={true}
                            label="Carto Base MAp"
                            
                            color={Cores.laranjaPadrao}
                            status={toggleCheckBox1 === true ? 'checked' : 'unchecked'}
                            onPress={() => {setToggleCheckBox1(!toggleCheckBox1)}}
                            />    
                        <Text style={{color:Cores.black, fontSize:16}}>Vendedor</Text>
                    </View>
                
            
                    <View  style={{flexDirection:'row', alignItems:'center', flex:1, alignSelf:'flex-end'}}>
                        <RadioButton
                            value={true}
                            label="Carto Base MAp"
                            color={Cores.laranjaPadrao}
                            status={toggleCheckBox2 === true ? 'checked' : 'unchecked'}
                            onPress={() => {setToggleCheckBox2(!toggleCheckBox2)}}
                            />    
                        <Text style={{color:Cores.black, fontSize:16}}>Comprador</Text>
                    </View>

                </View>

                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="Nome da empresa"
                    value={nomeEmpresa}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setNEmpresa(text)}
                />

                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="E-mail"
                    value={senha}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setSenha(text)}
                />
                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="Primeiro nome"
                    value={PrimeiroNome}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setNome(text)}
                />
                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="Sobrenome"
                    value={Sobrenome}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setSobrenome(text)}
                />
                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="Número de telefone"
                    value={tel}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setTel(text)}

                
                />
                <View style={{flexDirection:'row', alignItems:'center', margin:14}}>
                    <Checkbox
                        value={isChecked1}
                        onValueChange={setChecked1}
                        color={isChecked1 ? Cores.azulPadrao : undefined}
                        />
                    <Text style={{flex:1, margin:5}}>Quero receber ofertas e novidades da C&C por e-mail.</Text>
                </View>
                
                <View style={{flexDirection:'row', alignItems:'center', margin:14}}>
                    <Checkbox
                        value={isChecked2}
                        onValueChange={setChecked2}
                        color={isChecked2 ? Cores.azulPadrao : undefined}
                        />
                    <Text style={{flex:1, margin:5}}>Quero receber ofertas e novidades da C&C por SMS.</Text>
                </View>

                <View style={{flexDirection:'row', alignItems:'center', margin:14}}>
                    <Checkbox
                        value={isChecked3}
                        onValueChange={setChecked3}
                        color={isChecked3 ? Cores.azulPadrao : undefined}
                        />
                    <Text style={{flex:1, margin:5}}>Quero receber informações sobre meu pedido por WhatsApp.</Text>
                </View>

                <View style={{flexDirection:'row', alignItems:'center', margin:14}}>
                    <Checkbox
                        value={isChecked4}
                        onValueChange={setChecked4}
                        color={isChecked4 ? Cores.azulPadrao : undefined}
                        />
                    <Text style={{flex:1, margin:5}}>Permito que a C&C e/ou parceiro entre em contato caso necessário</Text>
                </View>

                
                
                <TextInput style={{...Estilos.card, elevation:1, marginStart:10, marginBottom:10, marginEnd:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza, color:Cores.black}}
                    placeholder="senha"
                    placeholderTextColor={Cores.cinza_escuro}
                    keyboardType='email-address'
                    value={email}// valor/texto do TextInpu
                            onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                            (text)=>setEmail(text)}
                    />
                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="confirmar senha"
                    keyboardType='visible-password'
                    value={senha}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setSenha(text)}
                />

                
                <View style={{alignItems:'center', flexDirection:'row'}}>
                    {!progtessbarsVisible&& 
                    <Button 
                        style={{margin:10, fontSize:16,fontWeight:'bold', paddingStart:14, paddingEnd:14, paddingTop:14, paddingBottom:14, flex:1}}
                        text={'Cadastrar-se'}
                        backgroundColor={Cores.laranjaPadrao}
                        />
                    }
                    
                </View>
                
            </SafeAreaView>
        </ScrollView>
        
    )
}