import React, { useState } from "react";
import { SafeAreaView, TextInput, TouchableHighlight, View, Text, Alert, ScrollView } from "react-native";
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons'
//import { ProgressBar } from '@react-native-community/progress-bar-android';
import Cores from '../util/Cores';
import Estilos from "../util/Estilos";
import Strings from "../util/Strings";
import Button from '../meus_componentes/Button'
//import auth from '@react-native-firebase/auth';
import { CommonActions } from '@react-navigation/native';

export default function(params){
    
   
    return(
        <SafeAreaView style={{width:'100%', height:'100%',alignItems:'center', justifyContent:'center'}}>
            <Text style={{color:Cores.azulPadrao, fontWeight:'bold', fontSize:18}}
                onPress={()=>{params.nav.navigate('Login')}}
            >Acesse sua conta ou registre-se</Text>
        </SafeAreaView>
        
    )
}