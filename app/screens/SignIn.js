import React, { useState } from "react";
import { SafeAreaView, TextInput, TouchableHighlight, View, Text, Alert, ScrollView, Image } from "react-native";
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons'
//import { ProgressBar } from '@react-native-community/progress-bar-android';
import Cores from '../util/Cores';
import Estilos from "../util/Estilos";
import Strings from "../util/Strings";
import Button from '../meus_componentes/Button'
//import auth from '@react-native-firebase/auth';
import { CommonActions } from '@react-navigation/native';

export default function(params){
    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    const [loginGoogle, setLogGoogle] = useState('')

    const [progtessbarsVisible, setPGBV] = useState(false)

    //logando
    /*
    function logar(){
        setPGBV(true)
        if(email == '' || senha == ''){
            Alert.alert(Strings.algo_errado, Strings.digite_email_senha)
            setPGBV(false)
        }else{
            auth()
            .signInWithEmailAndPassword(email, senha)
            .then(() => {
                console.log('User account created & signed in!');
                setPGBV(false)
                params.nav.dispatch(
                    CommonActions.navigate({
                      name: 'Splash',
                      params: {
                        //nav: params.nav,
                      },
                    })
                  );
                
            })
            .catch(error => {   
                var mensagem = error.code
                
                if (error.code === 'auth/invalid-email') {
                mensagem = Strings.email_errado
                }if(error.code === 'auth/wrong-password'){
                    mensagem = Strings.senha_incorreta
                }
                Alert.alert(Strings.algo_errado, mensagem)
                setPGBV(false)

            });
        }
    }*/

    return(
        <ScrollView style={{flex:1, height:'100%', backgroundColor:Cores.white}}>
            <SafeAreaView style={{flex:1, backgroundColor:Cores.white}}>
                
                <Image
                    style={{width:'70%', height:70, margin:'15%'}}
                    resizeMode='contain'
                    source={require('../imagens/logo.png')}
                />

                <TextInput style={{...Estilos.card, elevation:1, marginStart:10, marginBottom:10, marginEnd:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza, color:Cores.black}}
                    placeholder="E-mail"
                    placeholderTextColor={Cores.cinza_escuro}
                    keyboardType='email-address'
                    value={email}// valor/texto do TextInpu
                            onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                            (text)=>setEmail(text)}
                    />
                <TextInput style={{...Estilos.card, elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}
                    placeholderTextColor={Cores.cinza_escuro}
                    placeholder="Senha"
                    keyboardType='visible-password'
                    value={senha}// valor/texto do TextInpu
                        onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                        (text)=>setSenha(text)}
                />

                
                <View style={{alignItems:'center', flexDirection:'row'}}>
                    {!progtessbarsVisible&& 
                    <Button 
                        style={{margin:10, fontSize:16,fontWeight:'bold', paddingStart:14, paddingEnd:14, paddingTop:14, paddingBottom:14, flex:1}}
                        text={'Login'}
                        backgroundColor={Cores.laranjaPadrao}
                        />
                    }
                    
                </View>
                
                <View style={{flexDirection:'row', margin:10}}>
                    <Text style={{color:Cores.laranjaPadrao, fontSize:16, flex:1, textAlign:'left'}}>Esqueci minha senha</Text>
                    <Text style={{color:Cores.laranjaPadrao, fontSize:16, flex:1, textAlign:'right'}}>Esqueci meu email</Text>
                </View>

                <View style={{...Estilos.card, flexDirection:'row', justifyContent:'center', alignItems:'center', elevation:1, margin:10, padding:10, borderRadius:10, borderWidth:1, borderColor:Cores.cinza,color:Cores.black, }}>
                    <Image
                        style={{width:24, height:24, resizeMode:'contain',}}
                        source={require('../imagens/google_icon.png')}
                    />
                    <TextInput
                        style={{flex:1, marginStart:4, fontSize:16, textAlign:'center'}}
                        placeholderTextColor={Cores.cinza_escuro}
                        placeholder="LOGIN COM O GOOGLE"
                        value={loginGoogle}// valor/texto do TextInpu
                            onChangeText={//evento que pega o o texto digitado e põe no estado 'nome' para poder ser atualizado
                            (text)=>setLogGoogle(text)}
                    />
                </View>

                <Text style={{fontWeight:'bold', fontSize:16, alignSelf:'center', marginTop:20}}>Ainda não tem uma conta?</Text>
                <Text style={{color:Cores.laranjaPadrao, fontSize:18,fontWeight:'bold', flex:1, textAlign:'center',marginTop:6}}
                    onPress={()=>{
                        params.nav.navigate('Cadastrese')
                    }}
                >Cadastre-se</Text>
                
            </SafeAreaView>
        </ScrollView>
        
    )
}