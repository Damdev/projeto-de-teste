

//deve ser usado componente de classe
//aqui mesmo conceito de java
class Cores{
    static purple_200 = '#FFBB86FC'
    static purple_500 = '#1A91FA'
    static purple_700 = '#047BE3'
    static teal_200 = '#FF000000'
    static teal_700 = '#FF018786'
    static black = '#000000'
    static cinza = '#D5D4D4'
    static cinza_escuro = '#989797'
    static cinza_mais_escuro = '#616161'
    static white = '#FFFFFFFF'
    static azul_escuro = "#060480"
    static azul_claro = '#B2D9FB'
    static azul_branco = '#CBE1F4'
    static vermelho = '#FF0000'
    static verde = '#008000'
    static verde_claro = '#7CE011'
    static translucido = '#70000000'
    static laranja = '#FF7F00'
    static laranja2 = '#FFA500'
    static amarelo = '#FFFF00'

    //cores do projeto
    static laranjaPadrao = "#fa6403"
    static azulPadrao = "#023c7c"

}
export default Cores