import UserCliente from "../interacoes_banco/registra_users/UserCliente";

export default class{
    userCliente = new UserCliente; //cliente que deixou a avaliação
    txtAvaliação; //texto da avaliação do cliente
    estrelasAval = 1; //estrelas dadas pelo cliente
    data; //data da avaliação

}