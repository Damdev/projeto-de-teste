import GetCidades from "./GetCidades"
import Strings from "./Strings"

export default class{
    static s = Strings
    static estadosBR =["SELECIONE","AC", "AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"]
    
    static dias = [Strings.segunda_feira, Strings.ter_a_feira, Strings.quarta_feira, Strings.quinta_feira, Strings.sexta_feira, Strings.s_bado, Strings.domingo]
    
    static horas=["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    static minutos =["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
        
}