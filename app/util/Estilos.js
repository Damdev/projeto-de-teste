import {StyleSheet} from 'react-native';//importe os componentes a ser utilizados no projeto
import Cores from './Cores.js';
//aqui vai os estilos
export default StyleSheet.create({
    itensGaveta:{
        borderBottomColor: Cores.cinza,
        borderBottomWidth:1,
    },

    textBold:{
        fontWeight:'bold',
        color:Cores.black
    },
    textBold18:{
        fontWeight:'bold',
        color:Cores.black,
        fontSize:18
    },
    card: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 2,
        backgroundColor: 'white',
        borderRadius:2
      }

})