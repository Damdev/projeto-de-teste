import LatLong from "./LatLong";

export default class{
    id = 1;
    destinatario = "";
    rua = "";
    nº= "";
    referencia= "";
    bairro= "";
    cidade= "";
    estado= "";
    latLong = new LatLong();
    cep= "";
    isAtual = false;
}