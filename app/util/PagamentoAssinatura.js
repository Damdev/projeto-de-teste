import ObjetoStatic from  './ObjetosStatics.js'

export default class{
    id = null; //id da assinatura do cliente
    status= null;
    reason = ObjetoStatic.gratis; //nome do plano assinado
    payer_id = null;
    back_url= null;
    collecto_id= null;
    application_id= null;
    date_created= null;
    las_modified= null;
    preapproval_id= null;
    preapproval_plan_id= null; //id do plano assinado
    next_payment_date= null;
    payment_method_id= null;
    payer_first_name= null;
    payer_last_name= null;

    valorMensal= null;
    beneficios = [];

}