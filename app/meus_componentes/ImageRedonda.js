import React from "react";
import { View, Image } from "react-native";
import Cores from "../util/Cores";

export default function(params){
    const height = params.height
    const width = params.width
    const urlFoto = params.urlFoto
    return(
        <Image  
            source={{uri:urlFoto}} 
            style={{...params.style,height:height, width:width,borderRadius:height, borderColor:Cores.cinza, borderWidth:1, backgroundColor:Cores.white}} 
            resizeMethod="resize" //rendimensiona a imagem de acordo com o tamanha da <Image/>
            
        />
        
    )
}