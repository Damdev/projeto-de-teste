import React from "react";
import { View } from "react-native";
import Cores from "../util/Cores";

export default function(){
    return(
        <View style={{height:1, width:'100%', margin:5, backgroundColor:Cores.cinza_escuro}}></View>
    )
}