import React, { useEffect, useState } from "react";
import { TouchableHighlight, Text } from "react-native";
import Cores from "../util/Cores";

export default function(params){
    const [text, setText] = useState('')
    const [colorText, setColorText] = useState(Cores.white)
    const [backgroundColor, setBackColor] = useState(Cores.purple_500)
    useEffect(()=>{
        if(params.text != undefined){
            setText(params.text.toUpperCase())
        }
        if(params.textColor != undefined){
            setColorText(params.textColor)
        }
        if(params.backgroundColor != undefined){
            setBackColor(params.backgroundColor)
        }
    })
    return(
        
        <Text style={{
             borderRadius:4, margin:2,
            backgroundColor:backgroundColor, 
            color:colorText, 
            paddingTop:8, 
            paddingBottom:8, 
            paddingStart:16, 
            paddingEnd:16, ...params.style,
            textAlign:'center'
            }}
            onPress={()=>{params.onPress()}}   
        >
                {text}
        </Text>
        
    )
}