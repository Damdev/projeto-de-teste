import React, { useState, useEffect } from "react";
import { FlatList, View, Modal, Text, Button, StyleSheet, ScrollView, } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import Arrays from "../util/Arrays";
import Cores from "../util/Cores";
import Estilos from "../util/Estilos";
import Card from "./CardView";
import IconAntD from 'react-native-vector-icons/AntDesign'

//não uso esse componente (decidi usar o react-native-picker q é mais estavel)
export default function(params){
    const [visivel, setVisivel] = useState(false)
    
    const [listItems, setLE] = useState(params.array)
    const [itemAtual, setItemAtual] = useState(listItems[0])

    useEffect(()=>{
        setLE(params.array)
        
    })
    return(
        <View >

            <TouchableHighlight
                onPress={()=>{
                    setVisivel(true)
                    console.log("lista", params.array);
                }}
                underlayColor={Cores.white}
            >   
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text  style={{marginEnd:20, marginStart:10, color:Cores.black}}>{itemAtual}</Text>
                    <IconAntD name="caretdown" color={Cores.cinza_mais_escuro} size={12}/>
                </View>
                
            </TouchableHighlight>
                

            
            
            <Modal
                animationType='fade'// o modal entra com o tipo de animação indicado
                transparent={true} //se fica transparente ou n
                visible={visivel} //visível ou n
                onRequestClose={()=>{setVisivel(false)}}
            >
                <View style={{position:'absolute', backgroundColor:Cores.black, opacity:0.5, width:'100%', height:'100%'}}></View>
                
                <ScrollView
                    style={{...Estilos.card,margin:20, backgroundColor:Cores.white}}>

                
                    {listItems.map((item)=>
                        
                    <Text
                        key={item} 
                        onPress={()=>{
                            params.clicado(item)
                            setItemAtual(item)
                            setVisivel(false)
                        }}
                        
                        style={{textAlign:'center', alignSelf:'center', padding:2, marginStart:6, marginTop:10, marginBottom:10}}>{item}</Text>
                
                    )}
                </ScrollView>
                
                
            </Modal>
        </View>
    )
}

 


const estilo = StyleSheet.create({
    modal:{
        backgroundColor:"#000",
        margin:20,
        padding:20,
        borderRadius:20,
        elevation:10,
    },
    txtmodal:{
        color:'#fff'
    }
})